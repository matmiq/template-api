const jwt = require("jsonwebtoken");
const express = require("express");
const cors = require("cors");
const { v4: uuid } = require("uuid");

module.exports = function (corsOptions, { stanConn, mongoClient, secret }) {
  const api = express();

  api.use(express.json());
  api.use(cors(corsOptions));

  api.get("/", (req, res) => res.json("Hello, World!"));

  /* ******************* */
  /* YOUR CODE GOES HERE */
  /* ******************* */

  // Criação
  api.post("/users", (req, res) => {

    const user = req.body;
    const keys = Object.keys(user);
    const reemail = /\S+@\S+\.\S+/;
    const repass = /^[a-z0-9]+$/i;
    const passsize = user.password.length;
    let flag = 1;
    
    //Verificando se todos os campos estão presentes

    for(let i = 0; i < keys.length; i++){
      if(user[keys[i]] == '' || user[keys[i]].length <= 0){
        //erro 400 (BAD REQUEST)
        res.status(400).json({"error": "Request body had missing field {"+keys[i]+"}"});
        flag = -1;
      }
    }

    //Verificando se o email é escrito como string@string.string
    
    if(!reemail.test(user.email)){
      //erro 400 (BAD REQUEST)
      res.status(400).json({"error": "Request body had malformed field {email}"});
      flag = -1;
    }

    //Verificando se o password é alphanumeric e com tamanho entre 8 e 32 caracteres
   
    if((!repass.test(user.password)) || passsize < 8 || passsize > 32){
      //erro 400 (BAD REQUEST)
      res.status(400).json({"error": "Request body had malformed field {password}"});
      flag = -1;
    }

    //Verificar se password == passwordConfirmation

    if(user.password != user.passwordConfirmation){
      //erro 422 (UNPROCESSABLE ENTITY)
      res.status(422).json({"error": "Password confirmation did not match"});
      flag = -1;
    }

    //Verificando se o novo usuário passou nos testes e será criado 

    if(flag === 1){
      let id = uuid();
      let novoUsuario =
      {"user":
        { "id": id,
          "name": user.name, 
          "email": user.email
        } 
      };
      //201 (CREATED)
      res.status(201).json(novoUsuario); 
      let userCreated = {
        "eventType": 
        "UserCreated", 
        "entityId": id, 
        "entityAggregate": novoUsuario
      };
      // emitir um evento UserCreated no tópico users
      stanConn.publish('users', userCreated);
      mongoClient.db().collection("user").findOne({"id": id});
    }
    
  });

  // Deleção
  api.delete("/users/:uuid", (req, res) => {
    let flag = 1;
    const tokenBearer = req.get("Authentication");
    // Authentication: Bearer <token> (limpar o Bearer)
    const token = tokenBearer.replace('Bearer ', '');
    const userId = req.params.uuid;
    const tokenDecoded = jwt.verify(token, secret);

    // Verificar a presença do cabeçalho Authentication na requisição
    if(!(tokenBearer)){
      // erro 401 (UNAUTHORIZED)
      res.status(401).json({"error": "Access Token not found"});
      flag = -1;
    }
    
    // Verificar se o UUID presente dentro do token é igual ao UUID informado como parâmetro na rota
    if(userId != tokenDecoded.id){
      // erro 403 (FORBIDDEN)
      res.status(403).json({"error": "Access Token did not match User ID"});
      flag = -1;
    }

    // Deletar o usuário
    if(flag === 1){
      // 200 (OK)
      res.status(200).json({"id": userId});
      
      let userDeleted = {
        "eventType": "UserDeleted",
        "entityId": userId,
        "entityAggregate": {}
      };
      // emitir um evento UserDeleted no tópico users
      stanConn.publish('users', userDeleted);
      mongoClient.db().collection("id").findOne(userId);
    }

  });

  return api;
};
