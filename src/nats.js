const stan = require("node-nats-streaming");

module.exports = function (mongoClient) {
  const conn = stan.connect("test-cluster", "test", {
    url: process.env.BROKER_URL,
  });

  conn.on("connect", () => {
    console.log("Connected to NATS Streaming");

    /* ******************* */
    /* YOUR CODE GOES HERE */
    /* ******************* */


    const opts = conn.subscriptionOptions().setStartWithLastReceived();
    const userSubs = conn.subscribe('users', opts);
    
    userSubs.on('message', (msg) => {
      const userMsg = JSON.parse(msg.getData());
      const { 
        eventType: typeMsg, 
        entityId: idMsg, 
        entityAggregate: userAggregateMsg
      } = userMsg;
      if(typeMsg === "UserCreated"){
        //Criar usuário no MongoDb
      }
      if(typeMsg === "UserDeleted"){
        //Deletar usuário do MongoDb
      }

    });
  });

  return conn;
};
